package com.example.cakeeditor

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.widget.ImageView
import com.example.cakeeditor.utils.Point
import com.example.cakeeditor.utils.Saving
import kotlinx.android.synthetic.main.activity_spline.*


class SplineActivity : AppCompatActivity() {

    private var pointBitmap: Bitmap? = null
    private var resultBitmap: Bitmap? = null
    private val points = ArrayList<Point>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spline)

        val imageUri = Uri.parse(intent.getStringExtra("imageSpline"))
        val imageBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
        pointBitmap = imageBitmap
        imageSplineView.setImageBitmap(imageBitmap)

        lineButton.setOnClickListener {
            if (points.size > 1) {
                resultBitmap = buildLines(pointBitmap)
                imageSplineView.setImageBitmap(resultBitmap)
            }
        }

        splineButton.setOnClickListener {
            if (points.size > 1) {
                Thread(Runnable {
                    val (p1, p2) = computeControlPoints(points)
                    resultBitmap = drawSpline(pointBitmap, points, p1, p2)
                    imageSplineView.post { imageSplineView.setImageBitmap(resultBitmap) }
                }).start()
            }
        }

        splineResetButton.setOnClickListener {
            pointBitmap = imageBitmap
            resultBitmap = imageBitmap
            points.clear()
            imageSplineView.setImageBitmap(resultBitmap)
        }

        imageSplineView.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            startActivityForResult(intent, 5)
        }

        imageSplineView.setOnTouchListener { v, event ->
            val action = event.action
            val x = event.x.toInt()
            val y = event.y.toInt()
            if  (action == MotionEvent.ACTION_DOWN) {
                pointBitmap = getPoint(v as ImageView, pointBitmap, x, y)
                resultBitmap = pointBitmap
                imageSplineView.setImageBitmap(pointBitmap)
            }
            true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_tool, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_ok -> {
            val returnIntent = Intent()
            val newUri = Uri.fromFile(Saving.saveTempFile(resultBitmap, getExternalFilesDir(Environment.DIRECTORY_PICTURES)))
            returnIntent.putExtra("result", newUri.toString())
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun getPoint(iv: ImageView, imageBitmap: Bitmap?, x: Int, y: Int): Bitmap? {
        val newBitmap: Bitmap
        if (imageBitmap != null && x >= 0 && y >= 0 && x < iv.width && y < iv.height) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            val projectedX =
                (x.toDouble() * (width.toDouble() / iv.width.toDouble())).toInt()
            val projectedY =
                (y.toDouble() * (height.toDouble() / iv.height.toDouble())).toInt()

            val r = 10
            if (projectedX in r until width - r && projectedY in r until height - r) {
                points.add(Point(projectedX, projectedY))

                newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                val srcPixels = IntArray(width * height)
                imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)
                newBitmap.setPixels(srcPixels, 0, width, 0, 0, width, height)
                for(i in 360 downTo 1){
                    val dx = (r* Math.cos(i.toDouble())).toInt()
                    val dy = (r* Math.sin(i.toDouble())).toInt()

                    val x1 = projectedX - dx
                    val y1 = projectedY + dy
                    val y2 = projectedY - dy

                    for(j in x1..(x1+2*dx)){
                        newBitmap.setPixel(j, y1, 0xFFFFFFFF.toInt())
                        newBitmap.setPixel(j, y2, 0xFFFFFFFF.toInt())
                    }
                }
                return newBitmap
            }
        }
        return imageBitmap
    }

    private fun buildLines(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height

            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)
            newBitmap.setPixels(srcPixels, 0, width, 0, 0, width, height)

            var i = 0
            val k = points.size
            while (i < k - 1) {
                var deltaX = (points[i + 1].x - points[i].x)
                if (deltaX < 0) deltaX *= -1
                var deltaY = (points[i + 1].y - points[i].y)
                if (deltaY < 0) deltaY *= -1

                val signX = if (points[i].x < points[i + 1].x) 1 else -1
                val signY = if (points[i].y < points[i + 1].y) 1 else -1

                var error = deltaX - deltaY

                var x1 = points[i].x
                val x2 = points[i + 1].x
                var y1 = points[i].y
                val y2 = points[i + 1].y

                while (x1 != x2 || y1 != y2) {
                    newBitmap.setPixel(x1, y1, 0xFFFFFFFF.toInt())
                    val error2 = error * 2
                    if (error2 > -deltaY) {
                        error -= deltaY
                        x1 += signX
                    }
                    if (error2 < deltaX) {
                        error += deltaX
                        y1 += signY
                    }
                }
                i++
            }
        }
        return newBitmap
    }

    private fun drawSpline(imageBitmap: Bitmap?, points: ArrayList<Point>, p1: Array<Point>, p2: Array<Point>): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height

            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)
            newBitmap.setPixels(srcPixels, 0, width, 0, 0, width, height)

            for (i in 0 until points.size - 1) {
                var t = 0.0
                while (t <= 1) {
                    val x =
                        (Math.pow((1 - t), 3.0) * points[i].x + 3 * (1 - t) * (1 - t) * t * p1[i].x +
                                3 * (1 - t) * t * t * p2[i].x + Math.pow(t, 3.0) * points[i + 1].x).toInt()
                    val y =
                        (Math.pow((1 - t), 3.0) * points[i].y + 3 * (1 - t) * (1 - t) * t * p1[i].y +
                                3 * (1 - t) * t * t * p2[i].y + Math.pow(t, 3.0) * points[i + 1].y).toInt()
                    t += 0.0001
                    if (x in 0 until width && y in 0 until height)
                    newBitmap.setPixel(x, y, 0xFFFFFFFF.toInt())
                }
            }
        }
        return newBitmap
    }


    private fun computeControlPoints(points: ArrayList<Point>): Pair<Array<Point>, Array<Point>>{
        val n = points.size - 1

        if (n == 1) {
            val p1 = Array(1) {Point()}
            val p2 = Array(1) {Point()}

            p1[0].x = (2 * points[0].x + points[1].x) / 3
            p1[0].y = (2 * points[0].y + points[1].y) / 3

            p2[0].x = 2 * p1[0].x - points[0].x
            p2[0].y = 2 * p1[0].y - points[0].y

            return Pair(p1, p2)
        }

        val p1 = Array(n) {Point()}
        val p2 = Array(n) {Point()}

        val a = DoubleArray(n)
        val b = DoubleArray(n)
        val c = DoubleArray(n)
        val d = Array(n) {DoubleArray(2)}

        a[0] = 0.0
        b[0] = 2.0
        c[0] = 1.0
        d[0][0] = (points[0].x + 2 * points[1].x).toDouble()
        d[0][1] = (points[0].y + 2 * points[1].y).toDouble()

        for (i in 1 until n - 1) {
            a[i] = 1.0
            b[i] = 4.0
            c[i] = 1.0
            d[i][0] = (4 * points[i].x + 2 * points[i + 1].x).toDouble()
            d[i][1] = (4 * points[i].y + 2 * points[i + 1].y).toDouble()
        }

        a[n - 1] = 2.0
        b[n - 1] = 7.0
        c[n - 1] = 0.0
        d[n - 1][0] = (8 * points[n - 1].x + points[n].x).toDouble()
        d[n - 1][1] = (8 * points[n - 1].y + points[n].y).toDouble()

        for (i in 1 until n) {
            val w = a[i] / b[i - 1]
            b[i] = b[i] - w * c [i - 1]
            d[i][0] = d[i][0] - w * d[i - 1][0]
            d[i][1] = d[i][1] - w * d[i - 1][1]
        }

        p1[n - 1].x = (d[n - 1][0] / b[n - 1]).toInt()
        p1[n - 1].y = (d[n - 1][1] / b[n - 1]).toInt()

        for (i in n - 2 downTo 0) {
            p1[i].x = ((d[i][0] - c[i] * p1[i + 1].x) / b[i]).toInt()
            p1[i].y = ((d[i][1] - c[i] * p1[i + 1].y) / b[i]).toInt()
        }

        for (i in 0 until n - 1) {
            p2[i].x = (2 * points[i + 1].x - p1[i + 1].x)
            p2[i].y = (2 * points[i + 1].y - p1[i + 1].y)
        }
        p2[n - 1].x = (0.5 * (points[n].x + p1[n - 1].x)).toInt()
        p2[n - 1].y = (0.5 * (points[n].y + p1[n - 1].y)).toInt()

        return Pair(p1, p2)
    }
}