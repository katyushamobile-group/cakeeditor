package com.example.cakeeditor

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.cakeeditor.utils.RecyclerViewAdapter
import com.example.cakeeditor.utils.Saving
import kotlinx.android.synthetic.main.activity_tool.*
import java.io.File
import java.io.IOException


class ToolActivity : AppCompatActivity() {

    var adapter: RecyclerViewAdapter? = null
    private var imageBitmap: Bitmap? = null
    private var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tool)

        imageUri = Uri.parse(intent.getStringExtra("imageUri"))
        imageBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
        imageView.setImageBitmap(imageBitmap)

        val toolIcons = ArrayList<Int>()
        toolIcons.add(R.drawable.ic_action_rotate)
        toolIcons.add(R.drawable.ic_action_filter)
        toolIcons.add(R.drawable.ic_action_scale)
        toolIcons.add(R.drawable.ic_action_a_star)
        toolIcons.add(R.drawable.ic_action_spline)
        toolIcons.add(R.drawable.ic_action_retouch)
        toolIcons.add(R.drawable.ic_action_unsharp_masking)
        toolIcons.add(R.drawable.ic_action_texture_filtering)

        val toolTitles = ArrayList<String>()
        toolTitles.add(getString(R.string.rotate))
        toolTitles.add(getString(R.string.filter))
        toolTitles.add(getString(R.string.scale))
        toolTitles.add(getString(R.string.a_star))
        toolTitles.add(getString(R.string.spline))
        toolTitles.add(getString(R.string.retouch))
        toolTitles.add(getString(R.string.usm))
        toolTitles.add(getString(R.string.texture))

        // set up the RecyclerView
        val recyclerView = findViewById<RecyclerView>(R.id.toolRecyclerView)
        val horizontalLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.layoutManager = horizontalLayoutManager
        adapter = RecyclerViewAdapter(this, toolIcons, toolTitles)
        adapter!!.setClickListener(object: RecyclerViewAdapter.ItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                when (adapter!!.getItem(position)) {
                    getString(R.string.rotate) -> {
                        val sendPictureIntent = Intent(applicationContext, RotationActivity::class.java)
                        imageUri?.also {
                            sendPictureIntent.putExtra("imageRotation", imageUri.toString())
                            startActivityForResult(sendPictureIntent,1)
                        }
                    }
                    getString(R.string.filter) -> {
                        val sendPictureIntent = Intent(applicationContext, ColorFilterActivity::class.java)
                        imageUri?.also {
                            sendPictureIntent.putExtra("imageFilter", imageUri.toString())
                            startActivityForResult(sendPictureIntent,2)
                        }
                    }
                    getString(R.string.scale) -> {
                        val sendPictureIntent = Intent(applicationContext, ScalingActivity::class.java)
                        imageUri?.also {
                            sendPictureIntent.putExtra("imageScaling", imageUri.toString())
                            startActivityForResult(sendPictureIntent,3)
                        }
                    }
                    getString(R.string.a_star) -> {
                        val sendPictureIntent = Intent(applicationContext, AStarActivity::class.java)
                        imageUri?.also {
                            sendPictureIntent.putExtra("AStar", imageUri.toString())
                            startActivityForResult(sendPictureIntent,5)
                        }
                    }
                    getString(R.string.spline) -> {
                        val sendPictureIntent = Intent(applicationContext, SplineActivity::class.java)
                        imageUri?.also {
                            sendPictureIntent.putExtra("imageSpline", imageUri.toString())
                            startActivityForResult(sendPictureIntent,6)
                        }
                    }
                    getString(R.string.retouch) -> {
                        val sendPictureIntent = Intent(applicationContext, RetouchActivity::class.java)
                        imageUri?.also {
                            sendPictureIntent.putExtra("imageRetouch", imageUri.toString())
                            startActivityForResult(sendPictureIntent,7)
                        }
                    }
                    getString(R.string.usm) -> {
                        val sendPictureIntent = Intent(applicationContext, UnsharpMaskingActivity::class.java)
                        imageUri?.also {
                            sendPictureIntent.putExtra("imageUSM", imageUri.toString())
                            startActivityForResult(sendPictureIntent,8)
                        }
                    }
                    getString(R.string.texture) -> {
                        val sendPictureIntent = Intent(applicationContext, TextureFilteringActivity::class.java)
                        imageUri?.also {
                            sendPictureIntent.putExtra("imageTexture", imageUri.toString())
                            startActivityForResult(sendPictureIntent,9)
                        }
                    }
                    else -> Toast.makeText(
                        applicationContext,
                        "Does not work",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
        recyclerView.adapter = adapter

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_action_bar, menu)
        return true
    }

    @SuppressLint("SimpleDateFormat")
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_save -> {
            try {
                Saving.saveInFolder(
                    imageBitmap,
                    File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/CakeEditor")
                )
                Toast.makeText(applicationContext, "Saved", Toast.LENGTH_SHORT).show()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //Toast.makeText(this, "IWannaDie", Toast.LENGTH_LONG).show()
        if (resultCode == Activity.RESULT_OK) {
            val uri = Uri.parse(data!!.getStringExtra("result"))
            imageView.setImageURI(uri)
            imageUri = uri
            imageBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
        }
    }

}
