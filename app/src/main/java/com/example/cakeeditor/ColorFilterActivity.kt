package com.example.cakeeditor

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import com.example.cakeeditor.utils.ColorFilter
import com.example.cakeeditor.utils.MatrixFilter
import com.example.cakeeditor.utils.RecyclerViewAdapter
import com.example.cakeeditor.utils.Saving
import kotlinx.android.synthetic.main.activity_color_filter.*


class ColorFilterActivity : AppCompatActivity() {

    var adapter: RecyclerViewAdapter? = null
    private var imageBitmap: Bitmap? = null
    private var filterImageBitmap: Bitmap? = null
    private var colorBitmap: Bitmap? = null
    private var resultImageBitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_color_filter)

        val imageUri = Uri.parse(intent.getStringExtra("imageFilter"))
        imageBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
        filterImageBitmap = imageBitmap
        resultImageBitmap = imageBitmap
        colorBitmap = imageBitmap
        imageFilterView.setImageBitmap(imageBitmap)

        brightnessSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                resultImageBitmap = ColorFilter.setBrightness(filterImageBitmap, i-250)
                imageFilterView.setImageBitmap(resultImageBitmap)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {
                filterImageBitmap = resultImageBitmap
            }
        })

        contrastSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                resultImageBitmap = ColorFilter.setContrast(filterImageBitmap, i-100)
                imageFilterView.setImageBitmap(resultImageBitmap)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {
                filterImageBitmap = resultImageBitmap
            }
        })

        resetFilterButton.setOnClickListener {
            filterImageBitmap = colorBitmap
            resultImageBitmap = colorBitmap
            brightnessSeekBar.progress = 250
            contrastSeekBar.progress = 100
            imageFilterView.setImageBitmap(resultImageBitmap)
        }


        val toolIcons = ArrayList<Int>()
        toolIcons.add(R.drawable.ic_main_image)
        toolIcons.add(R.drawable.ic_red_filter)
        toolIcons.add(R.drawable.ic_green_filter)
        toolIcons.add(R.drawable.ic_blue_filter)
        toolIcons.add(R.drawable.ic_black_white_filter)
        toolIcons.add(R.drawable.ic_negative_filter)
        toolIcons.add(R.drawable.ic_sepia_filter)
        toolIcons.add(R.drawable.ic_cold_filter)
        toolIcons.add(R.drawable.ic_warm_filter)
        toolIcons.add(R.drawable.ic_x_ray_filter)
        toolIcons.add(R.drawable.ic_edge_detector)

        val toolTitles = ArrayList<String>()
        toolTitles.add("No filter")
        toolTitles.add("Red")
        toolTitles.add("Green")
        toolTitles.add("Blue")
        toolTitles.add("BW")
        toolTitles.add("Negative")
        toolTitles.add("Sepia")
        toolTitles.add("Cold")
        toolTitles.add("Warm")
        toolTitles.add("X-ray")
        toolTitles.add("EdgeDetector")

        // set up the RecyclerView
        val recyclerView = findViewById<RecyclerView>(R.id.toolRecyclerView)
        val horizontalLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.layoutManager = horizontalLayoutManager
        adapter = RecyclerViewAdapter(this, toolIcons, toolTitles)
        adapter!!.setClickListener(object: RecyclerViewAdapter.ItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                Thread(Runnable {
                    when (adapter!!.getItem(position)) {
                        "No filter" -> filterImageBitmap = imageBitmap
                        "Red" -> filterImageBitmap = ColorFilter.redFilter(imageBitmap)
                        "Green" -> filterImageBitmap = ColorFilter.greenFilter(imageBitmap)
                        "Blue" -> filterImageBitmap = ColorFilter.blueFilter(imageBitmap)
                        "BW" -> filterImageBitmap = ColorFilter.blackWhiteFilter(imageBitmap)
                        "Negative" -> filterImageBitmap = ColorFilter.negativeFilter(imageBitmap)
                        "Sepia" -> filterImageBitmap = ColorFilter.sepiaFilter(imageBitmap)
                        "Cold" -> filterImageBitmap = ColorFilter.coldFilter(imageBitmap)
                        "Warm" -> filterImageBitmap = ColorFilter.warmFilter(imageBitmap)
                        "X-ray" -> filterImageBitmap = ColorFilter.xRayFilter(imageBitmap)
                        "EdgeDetector" -> filterImageBitmap = MatrixFilter.edgeDetectionFilter(imageBitmap)

                    }
                    imageFilterView.post {
                        resultImageBitmap = filterImageBitmap
                        colorBitmap = filterImageBitmap
                        imageFilterView.setImageBitmap(filterImageBitmap)
                        brightnessSeekBar.progress = 250
                        contrastSeekBar.progress = 100
                    }
                }).start()
            }
        })
        recyclerView.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_tool, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_ok -> {
            val returnIntent = Intent()
            val newUri = Uri.fromFile(Saving.saveTempFile(filterImageBitmap, getExternalFilesDir(Environment.DIRECTORY_PICTURES)))
            returnIntent.putExtra("result", newUri.toString())
            setResult(Activity.RESULT_OK, returnIntent)
           finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}
