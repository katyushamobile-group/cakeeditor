package com.example.cakeeditor

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.widget.ImageView
import com.example.cakeeditor.utils.Point
import com.example.cakeeditor.utils.Saving
import kotlinx.android.synthetic.main.activity_texture_filtering.*

class TextureFilteringActivity : AppCompatActivity() {

    private val points = ArrayList<Point>()
    private val newPoints = ArrayList<Point>()
    private var resultBitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_texture_filtering)

        val imageUri = Uri.parse(intent.getStringExtra("imageTexture"))
        var imageBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
        imageTextureView.setImageBitmap(imageBitmap)

        imageTextureView.setOnTouchListener { v, event ->
            val action = event.action
            val x = event.x.toInt()
            val y = event.y.toInt()
            if  (k < 6 && action == MotionEvent.ACTION_DOWN) {
                imageBitmap = getPoint(v as ImageView, imageBitmap, x, y)
                imageTextureView.setImageBitmap(imageBitmap)
            }
            true
        }

        applyTransformButton.setOnClickListener {
            if (imageBitmap != null && k == 6) {
                resultBitmap = affineTransform(imageBitmap, points, newPoints)
                imageTextureView.setImageBitmap(affineTransform(imageBitmap, points, newPoints))
                k = 0
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_tool, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_ok -> {
            val returnIntent = Intent()
            val newUri = Uri.fromFile(Saving.saveTempFile(resultBitmap, getExternalFilesDir(Environment.DIRECTORY_PICTURES)))
            returnIntent.putExtra("result", newUri.toString())
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private var k = 0
    private fun getPoint(iv: ImageView, imageBitmap: Bitmap?, x: Int, y: Int): Bitmap? {
        val newBitmap: Bitmap?
        if (imageBitmap != null && x >= 0 && y >= 0 && x < iv.width && y < iv.height) {
            val width = imageBitmap.width
            val height = imageBitmap.height

            val projectedX =
                (x.toDouble() * (width.toDouble() / iv.width.toDouble())).toInt()
            val projectedY =
                (y.toDouble() * (height.toDouble() / iv.height.toDouble())).toInt()

            if (width >= 10 && height >= 10 && projectedX in 0 until width && projectedY in 0 until height) {
                if (k < 3) points.add(Point(projectedX, projectedY))
                else newPoints.add(Point(projectedX, projectedY))
                k++
                newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                val srcPixels = IntArray(width * height)
                imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)
                newBitmap.setPixels(srcPixels, 0, width, 0, 0, width, height)
                newBitmap.setPixel(projectedX, projectedY, 0xFFFFFFFF.toInt())
                return newBitmap
            }
        }
        return imageBitmap
    }

    private fun affineTransform(imageBitmap: Bitmap, points: ArrayList<Point>, newPoints: ArrayList<Point>): Bitmap? {
        val transform = computeAffineTransform(points, newPoints)
        val width = imageBitmap.width
        val height = imageBitmap.height
        val srcPixels = IntArray(width * height)
        val newPixels = IntArray(width * height)
        imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)
        var x = 0
        var y = 0

        for (i in 0 until srcPixels.size) {
            val newX = (transform[0] * x + transform[1] * y + transform[2]).toInt()
            val newY = (transform[3] * x + transform[4] * y + transform[5]).toInt()

            if (newX in 0 until width && newY in 0 until height) {
                val j = newX + newY * width
                newPixels[j] = srcPixels[i]
            } else {
                newPixels[i] = 0
            }
            x++
            if (x == width) {
                x = 0
                y++
            }
        }
        val newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        return newBitmap
    }

    private fun computeAffineTransform(points: ArrayList<Point>, newPoints: ArrayList<Point>): DoubleArray {
        val size = 6
        val matrix = Array(size) { Array(size) { 0.0 } }
        matrix[0] = arrayOf(points[0].x.toDouble(), points[0].y.toDouble(), 1.0, 0.0, 0.0, 0.0)
        matrix[1] = arrayOf(0.0, 0.0, 0.0, points[0].x.toDouble(), points[0].y.toDouble(), 1.0)
        matrix[2] = arrayOf(points[1].x.toDouble(), points[1].y.toDouble(), 1.0, 0.0, 0.0, 0.0)
        matrix[3] = arrayOf(0.0, 0.0, 0.0, points[1].x.toDouble(), points[1].y.toDouble(), 1.0)
        matrix[4] = arrayOf(points[2].x.toDouble(), points[2].y.toDouble(), 1.0, 0.0, 0.0, 0.0)
        matrix[5] = arrayOf(0.0, 0.0, 0.0, points[2].x.toDouble(), points[2].y.toDouble(), 1.0)

        val newX =
            arrayOf(newPoints[0].x.toDouble(), newPoints[0].y.toDouble(),
                newPoints[1].x.toDouble(), newPoints[1].y.toDouble(),
                newPoints[2].x.toDouble(), newPoints[2].y.toDouble())


        for (p in 0 until size) {

            var max = p
            for (i in p + 1 until size) {
                if (Math.abs(matrix[i][p]) > Math.abs(matrix[max][p])) {
                    max = i
                }
            }
            val temp = matrix[p]
            matrix[p] = matrix[max]
            matrix[max] = temp
            val t = newX[p]
            newX[p] = newX[max]
            newX[max] = t

            for (i in p + 1 until size) {
                val alpha = matrix[i][p] / matrix[p][p]
                newX[i] -= alpha * newX[p]
                for (j in p until size) {
                    matrix[i][j] -= alpha * matrix[p][j]
                }
            }
        }

        val transform = DoubleArray(size)
        for (i in size - 1 downTo 0) {
            var sum = 0.0
            for (j in i + 1 until size) {
                sum += matrix[i][j] * transform[j]
            }
            transform[i] = (newX[i] - sum) / matrix[i][i]
        }
        return transform
    }
}

