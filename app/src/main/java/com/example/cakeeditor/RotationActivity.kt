package com.example.cakeeditor

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.SeekBar
import com.example.cakeeditor.utils.Saving
import com.example.cakeeditor.utils.Scaling
import kotlinx.android.synthetic.main.activity_rotation.*

class RotationActivity : AppCompatActivity() {

    private var rotatedBitmap: Bitmap? = null
    private var angle = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rotation)

        val imageUri = Uri.parse(intent.getStringExtra("imageRotation"))
        var imageBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
        rotatedBitmap = imageBitmap
        imageRotationView.setImageBitmap(imageBitmap)

        //degreesText.setText(angle.toString())

        rotateImageLeftButton.setOnClickListener {
            imageBitmap = imageRotation90Left(imageBitmap)
            rotatedBitmap = imageBitmap
            rotateSeekBar.progress = 450
            degreesText.setText("0.0°")
            imageRotationView.setImageBitmap(imageBitmap)
        }
        rotateImageRightButton.setOnClickListener {
            imageBitmap = imageRotation90Right(imageBitmap)
            rotatedBitmap = imageBitmap
            rotateSeekBar.progress = 450
            degreesText.setText("0.0°")
            imageRotationView.setImageBitmap(imageBitmap)
        }

        degreesText.setOnClickListener {
            try {
                val angleText = degreesText.text.toString()
                angle = angleText.replace("°", "").toDouble()
                rotateSeekBar.progress = (angle * 10).toInt() + 450
            } catch (e: Exception) {
                degreesText.setText("0.0°")
            }
        }

        rotateSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            @SuppressLint("SetTextI18n")
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                angle = (i - 450) / 10.0
                degreesText.setText("$angle°")
                if (angle != 0.0) {
                    rotatedBitmap = imageRotationAtAngle(imageBitmap, angle)
                    imageRotationView.setImageBitmap(rotatedBitmap)
                }
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }

        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_tool, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_ok -> {
            val returnIntent = Intent()
            val newUri = Uri.fromFile(Saving.saveTempFile(rotatedBitmap, getExternalFilesDir(Environment.DIRECTORY_PICTURES)))
            returnIntent.putExtra("result", newUri.toString())
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun imageRotation90Left(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(height, width, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(height * width)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            var temp = width - 1
            var n = width - 1
            var k = 0
            for (i in 0 until srcPixels.size) {
                newPixels[i] = srcPixels[n]
                n += width
                k++
                if (k == height) {
                    temp--
                    n = temp
                    k = 0
                }
            }

            newBitmap.setPixels(newPixels, 0, height, 0, 0, height, width)
        }
        return newBitmap
    }

    private fun imageRotation90Right(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(height, width, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(height * width)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            var temp = width * (height - 1)
            var n = width * (height - 1)
            var k = 0
            for (i in 0 until srcPixels.size) {
                newPixels[i] = srcPixels[n]
                n -= width
                k++
                if (k == height) {
                    temp++
                    n = temp
                    k = 0
                }
            }

            newBitmap.setPixels(newPixels, 0, height, 0, 0, height, width)
        }
        return newBitmap
    }

    fun imageRotationAtAngle(imageBitmap: Bitmap?, angle: Double): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height

            val angleInRad = angle * Math.PI / 180.0
            val sinA = Math.sin(angleInRad)
            val cosA = Math.cos(angleInRad)
            val tanA2 = Math.tan(angleInRad / 2.0)

            val x0 = width / 2
            val y0 = height / 2

            val srcPixels = IntArray(width * height)
            var newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            var x = 0
            var y = 0

            for (i in 0 until srcPixels.size) {
                //val newX = (cosA * (x - x0) - sinA * (y - y0) + x0).toInt()
                //val newY = (sinA * (x - x0) + cosA * (y - y0) + y0).toInt()

                var newX = x
                var newY = y

                newX = (newX - Math.rint(tanA2 * (newY - y0))).toInt()
                newY = (Math.rint(sinA * (newX - x0)) + newY).toInt()
                newX = (newX - Math.rint(tanA2 * (newY - y0))).toInt()

                if (newX in 0 until width && newY in 0 until height) {
                    val j = newX + newY * width
                    newPixels[j] = srcPixels[i]
                } else {
                    newPixels[i] = 0
                }
                x++
                if (x == width) {
                    x = 0
                    y++
                }
            }

            val bigWidth = (Math.abs(cosA * width) + Math.abs(sinA * height)).toInt()
            val bigHeight = (Math.abs(sinA * width) + Math.abs(cosA * height)).toInt()

            val ratio = width.toDouble() / height.toDouble()
            val rotatedRatio = bigWidth.toDouble() / bigHeight.toDouble()

            val totalHeight = if (ratio < 1) width.toDouble() / rotatedRatio else height.toDouble()

            val newHeight = (totalHeight / (ratio * Math.abs(sinA) + Math.abs(cosA))).toInt()
            val newWidth = (newHeight * ratio).toInt()

            val newX0 = ((width - newWidth) / 2)
            val newY0 = ((height - newHeight) / 2)

            val tempPixels = IntArray(newWidth * newHeight)
            var k = newX0 + newY0 * width
            var n = 0
            for (i in 0 until tempPixels.size) {
                tempPixels[i] = newPixels[k + n]
                n++
                if (n == newWidth) {
                    n = 0
                    k += width
                }
            }

            newPixels = Scaling.resizeBilinear(tempPixels, newWidth, newHeight, width, height)
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
            /*val tempBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            tempBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
            newBitmap = Bitmap.createBitmap(tempBitmap, newX0, newY0, newWidth, newHeight)*/
        }
        return newBitmap
    }
}
