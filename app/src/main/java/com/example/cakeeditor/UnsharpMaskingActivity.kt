package com.example.cakeeditor

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.SeekBar
import android.widget.Toast
import com.example.cakeeditor.utils.MatrixFilter
import com.example.cakeeditor.utils.Saving
import kotlinx.android.synthetic.main.activity_unsharp_masking.*

class UnsharpMaskingActivity : AppCompatActivity() {

    private var imageBitmap: Bitmap? = null
    private  var resultBitmap: Bitmap? = null
    var radius = 0.0
    var amount = 0.0
    var threshold = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unsharp_masking)

        val imageUri = Uri.parse(intent.getStringExtra("imageUSM"))
        imageBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
        resultBitmap = imageBitmap
        imageUSMView.setImageBitmap(imageBitmap)

        radiusText.setText(radius.toString())
        amountText.setText(amount.toString())
        thresholdText.setText(threshold.toString())

        radiusSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                radius = i / 100.0 + 0.01
                radiusText.setText(radius.toString())
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        amountSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                amount = i / 100.0
                amountText.setText(amount.toString())
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        thresholdSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                threshold = i
                thresholdText.setText(threshold.toString())
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        radiusText.setOnClickListener {
            try {
                radius = radiusText.text.toString().toDouble()
                radiusSeekBar.progress = (radius * 100.0 - 0.01).toInt()
            } catch (e: Exception) {
                radiusText.setText((0.0).toString())
            }
        }
        amountText.setOnClickListener {
            if (amountText.text.toString() != "") {
                amount = amountText.text.toString().toDouble()
                amountSeekBar.progress = (amount * 100.0).toInt()
            }
        }
        thresholdText.setOnClickListener{
            if (thresholdText.toString() != "") {
                thresholdSeekBar.progress = (thresholdText.text.toString()).toInt()
            }
        }

        applyUSMButton.setOnClickListener{
            if (radius == 0.0 && amount == 0.0 && threshold == 0)
                Toast.makeText(applicationContext, "Enter the parameters", Toast.LENGTH_SHORT).show()
            else {
                Thread(Runnable {
                    resultBitmap = MatrixFilter.unsharpMashing(imageBitmap, amount, radius, threshold)
                    imageUSMView.post {
                    imageUSMView.setImageBitmap(resultBitmap)
                    }
                }).start()
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_tool, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_ok -> {
            val returnIntent = Intent()
            val newUri = Uri.fromFile(Saving.saveTempFile(imageBitmap, getExternalFilesDir(Environment.DIRECTORY_PICTURES)))
            returnIntent.putExtra("result", newUri.toString())
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}