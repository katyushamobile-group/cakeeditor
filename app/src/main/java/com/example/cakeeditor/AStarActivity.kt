package com.example.cakeeditor

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.widget.Button
import android.widget.ImageView
import android.widget.PopupMenu
import com.example.cakeeditor.utils.Point
import com.example.cakeeditor.utils.Vertex
import kotlinx.android.synthetic.main.activity_astar.*
import kotlin.math.sqrt

class AStarActivity : AppCompatActivity() {

    var imageBitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_astar)

        imageBitmap = Bitmap.createBitmap(30, 30, Bitmap.Config.ARGB_8888)
        imageBitmap!!.eraseColor(ContextCompat.getColor(applicationContext ,R.color.colorPrimaryLight))
        aStarFieldView.setImageBitmap(imageBitmap)

        aStarFieldView.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            startActivityForResult(intent, 5)
        }

        heuristicView.text = getString(R.string.manhattan)

        startPointButton.setOnClickListener {
            choose = 0
            visitStart = 1
        }

        endPointButton.setOnClickListener {
            choose = 1
            visitEnd = 1
        }

        drawLabirintButton.setOnClickListener {
            choose = 2
        }

        startAStarButton.setOnClickListener {
            if(visitStart == 1 && visitEnd == 1){
                imageBitmap = aStar(imageBitmap)
                aStarFieldView.setImageBitmap(imageBitmap)
            }
        }

        resetAStarButton.setOnClickListener {
            imageBitmap!!.eraseColor(ContextCompat.getColor(applicationContext ,R.color.colorPrimaryLight))
            aStarFieldView.setImageBitmap(imageBitmap)
            choose = -1
            chooseHeuristic = 1
            heuristicView.text = getString(R.string.manhattan)
            visitStart = -1; visitEnd = -1
            start_x = -1; start_y = -1; end_x = -1; end_y = -1
            scoreView.text = ""
        }

        val button = findViewById<Button>(R.id.heuristicButton)
        button.setOnClickListener {
            val popupMenu = PopupMenu(this, button)
            popupMenu.menuInflater.inflate(R.menu.menu_heuristic, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.manhattan -> {
                        chooseHeuristic = 1
                        heuristicView.text = getString(R.string.manhattan)
                    }

                    R.id.euclid -> {
                        chooseHeuristic = 2
                        heuristicView.text = getString(R.string.euclid)
                    }
                }
                true
            }
            popupMenu.show()
        }

            aStarFieldView.setOnTouchListener { v, event ->
            val action = event.action
            val x = event.x.toInt()
            val y = event.y.toInt()

            // начальная и конечная точки
            if (action == MotionEvent.ACTION_DOWN && (choose == 0 || choose == 1)) {
                imageBitmap = getPoint(v as ImageView, imageBitmap, x, y)
                aStarFieldView.setImageBitmap(imageBitmap)
                choose = -1
            }

            // рисуем лабиринт
            if ((action == MotionEvent.ACTION_MOVE || action == MotionEvent.ACTION_DOWN ) && choose == 2) {
                imageBitmap = getPoint(v as ImageView, imageBitmap, x, y)
                aStarFieldView.setImageBitmap(imageBitmap)
            }

            true
        }


    }

    // для определения того, что мы рисуем
    private var choose = -1
    // выбор эвристики 1-манхетон, 2-евклид
    private var chooseHeuristic = 1
    
    var visitStart = -1; var visitEnd = -1
    // координаты начала и конца
    var start_x = -1; var start_y = -1; var end_x = -1; var end_y = -1

    private fun getPoint(iv: ImageView, imageBitmap: Bitmap?, x: Int, y: Int): Bitmap? {
        val newBitmap: Bitmap?
        if (imageBitmap != null && x >= 0 && y >= 0 && x < iv.width && y < iv.height) {
            val width = imageBitmap.width
            val height = imageBitmap.height

            val projectedX =
                (x.toDouble() * (width.toDouble() / iv.width.toDouble())).toInt()
            val projectedY =
                (y.toDouble() * (height.toDouble() / iv.height.toDouble())).toInt()

            if (width >= 10 && height >= 10 && projectedX in 0 until width && projectedY in 0 until height) {
                newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

                val srcPixels = IntArray(width * height)
                imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)
                newBitmap.setPixels(srcPixels, 0, width, 0, 0, width, height)
                if(choose == 2) newBitmap.setPixel(projectedX, projectedY, 0xFFFFFFFF.toInt())  // белые точки
                else if(choose == 0){
                    newBitmap.setPixel(projectedX, projectedY, Color.GREEN)
                    start_x = projectedX
                    start_y = projectedY
                }
                else if(choose == 1){
                    newBitmap.setPixel(projectedX, projectedY, Color.RED)
                    end_x = projectedX
                    end_y = projectedY
                }
                return newBitmap
            }
        }
        return imageBitmap
    }




    @SuppressLint("SetTextI18n")
    fun aStar(imageBitmap: Bitmap?): Bitmap? {
        choose = -1

        val width = imageBitmap!!.width
        val height = imageBitmap.height

        val newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

        val srcPixels = IntArray(width * height)
        imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)
        newBitmap.setPixels(srcPixels, 0, width, 0, 0, width, height)


        val move_x = intArrayOf(-1, 0, 1, 0)
        val move_y = intArrayOf(0, 1, 0, -1)


        val points = Array(width) { Array(height) {Vertex()} }

        for(i in 0 until width)
            for(j in 0 until height){
                points[i][j].x = i
                points[i][j].y = j
                points[i][j].visited = 0
                points[i][j].to_watch = 0
                points[i][j].dist_from_start = 10000000
                points[i][j].func = 100000000
            }

        points[start_x][start_y].to_watch = 1
        points[start_x][start_y].parent_x = 0
        points[start_x][start_y].parent_y = 0
        points[start_x][start_y].dist_from_start = 0
        if(chooseHeuristic == 1)
            points[start_x][start_y].func = points[start_x][start_y].dist_from_start + manhattanDistance(start_x, start_y, end_x, end_y)
        else
            points[start_x][start_y].func = points[start_x][start_y].dist_from_start + euclidDistance(start_x, start_y, end_x, end_y).toInt()
        
        
        //количество итераций
        var k = 0
        
        while(true){
            k++
            if(k == 3000) break

            var cond = true
            for (i in 0 until height)
                for (j in 0 until width)
                    if (points[i][j].to_watch == 0) {
                        cond = false
                        break
                    }
            if (cond)
                break

            var minVertex = 10000000000
            var x = 0
            var y = 0
            for (i in 0 until width)
                for (j in 0 until height)
                    if (points[i][j].to_watch == 1 && points[i][j].func < minVertex) {
                        minVertex = points[i][j].func.toLong()
                        x = i
                        y = j
                    }

            if (x == end_x && y == end_y) {
                break
            }

            points[x][y].to_watch = 0
            points[x][y].visited = 1

            for (move in 0..3) {
                if ((x + move_x[move]) in 0 until width && (y + move_y[move]) in 0 until height) {

                    val new_x = x + move_x[move]
                    val new_y = y + move_y[move]

                    //matrix[new_x][new_y] != -1
                    if (newBitmap.getPixel(new_x,new_y) != 0xFFFFFFFF.toInt()) {
                        val tentative_score = points[x][y].dist_from_start + 1

                        if (points[new_x][new_y].visited == 1 && tentative_score >= points[new_x][new_y].dist_from_start)
                            continue
                        if (points[new_x][new_y].visited == 0 || tentative_score < points[new_x][new_y].dist_from_start) {
                            points[new_x][new_y].parent_x = x
                            points[new_x][new_y].parent_y = y
                            points[new_x][new_y].dist_from_start = tentative_score
                            if(chooseHeuristic == 1)
                                points[new_x][new_y].func = points[new_x][new_y].dist_from_start + manhattanDistance(new_x, new_y, end_x, end_y)
                            else
                                 points[new_x][new_y].func = points[new_x][new_y].dist_from_start + euclidDistance(new_x, new_y, end_x, end_y).toInt()
                            points[new_x][new_y].to_watch = 1
                        }
                    }
                }
            }
        }


        if( k != 3000) {
            val answers = Array(1000000) { Point() }
            var x = end_x
            var y = end_y
            var answer = 1
            var f = true
            while (x != start_x || y != start_y) {
                if (answer + 2 > 1000000) {
                    f = false
                    break
                }
                answers[answer].x = x
                answers[answer].y = y
                val new_x = points[x][y].parent_x
                val new_y = points[x][y].parent_y

                x = new_x
                y = new_y
                answer++
            }
            answers[answer].x = start_x
            answers[answer].y = start_y

            if (f) {
                for (i in answer - 1 downTo 2) {
                    newBitmap.setPixel(answers[i].x, answers[i].y, Color.YELLOW)
                }
                answer--
                scoreView.text = getString(R.string.path_score) + answer
            }
        }
        
        return newBitmap
        
    }

    private fun manhattanDistance(x1: Int, y1: Int, x2: Int, y2: Int): Int {
        return Math.abs(x2 - x1) + Math.abs(y2 - y1)
    }
    
    private fun euclidDistance(x1: Int, y1: Int, x2: Int, y2: Int): Double {
        return sqrt(((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)).toDouble())
    }

}
