package com.example.cakeeditor.utils

import android.annotation.SuppressLint
import android.graphics.Bitmap
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object Saving {
    @SuppressLint("SimpleDateFormat")
    fun saveTempFile(imageBitmap: Bitmap?, pathFile: File?): File? {
        var tempFile: File? = null
        if (imageBitmap != null) {
            tempFile = File.createTempFile(
                getNameFile(),
                ".jpg",
                pathFile
            )
            try {
                val out = FileOutputStream(tempFile)
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
                out.flush()
                out.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return tempFile
    }

    fun saveInFolder(imageBitmap: Bitmap?, filePath: File?) {
        val file: File?
        if (imageBitmap != null && filePath != null) {
            if (!filePath.exists()) filePath.mkdirs()
            val fileName = getNameFile()
            file = File(filePath, fileName)
            //MediaStore.Images.Media.insertImage(this.contentResolver, imageBitmap , "MyCake_$timeStamp", "MyCake_$timeStamp")
            val out = FileOutputStream(file)
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
            out.flush()
            out.close()
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun createTempFile(storageDir: File): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        return File.createTempFile(
            "MyCake_${timeStamp}_", ".jpg", storageDir)
    }

    @SuppressLint("SimpleDateFormat")
    private fun getNameFile(): String{
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        return "MyCake_$timeStamp.jpg"
    }
}