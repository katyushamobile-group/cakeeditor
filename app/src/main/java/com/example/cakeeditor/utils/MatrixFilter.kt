package com.example.cakeeditor.utils

import android.graphics.Bitmap

object MatrixFilter {
    fun edgeDetectionFilter(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            val newWidth = width + 2
            val newHeight = height + 2
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)

            newBitmap = blurFilter(ColorFilter.blackWhiteFilter(imageBitmap),1.0)
            newBitmap!!.getPixels(srcPixels, 0, width, 0, 0, width, height)

            val gX = Array(3) {Array(3) {0L} }
            gX[0] = arrayOf(-1, 0, 1)
            gX[1] = arrayOf(-2, 0, 2)
            gX[2] = arrayOf(-1, 0, 1)

            val gY = Array(3) {Array(3) {0L} }
            gY[0] = arrayOf(1, 2, 1)
            gY[1] = arrayOf(0, 0, 0)
            gY[2] = arrayOf(-1, -2, -1)

            for (i in 0 until width*height) {
                srcPixels[i] = srcPixels[i]
            }

            val matrix = Array(newHeight) {Array(newWidth) {0}}
            var n = 0
            for (i in 1..height) {
                for (j in 1..width) {
                    matrix[i][j] = srcPixels[n]
                    n++
                }
            }

            matrix[0][0] = srcPixels[0]
            matrix[0][newWidth-1] = srcPixels[height*width-1]
            matrix[newHeight-1][0] = srcPixels[height*width-width]
            matrix[newHeight-1][newWidth-1] = srcPixels[height*width-1]

            for (i in 0 until width) {
                matrix[0][i+1] = srcPixels[i]
            }
            for (i in 1 until width+1) {
                matrix[newHeight-1][newWidth-i-1] = srcPixels[height*width-i-1]
            }
            for (i in 0 until height) {
                matrix[i+1][0] = srcPixels[i*width]
            }
            for (i in 0 until height) {
                matrix[i+1][newWidth-1] = srcPixels[i*width+width-1]
            }

            var redX = 0L
            var greenX = 0L
            var blueX = 0L
            var redY = 0L
            var greenY = 0L
            var blueY = 0L
            n = 0
            val size = gX.size
            for (i in 0 until height) {
                for (j in 0 until width) {
                    for (k in 0 until size) {
                        for (l in 0 until size) {
                            redX += (((matrix[i+k][j+l] and 0x00FF0000) shr 16)*gX[k][l])
                            greenX += (((matrix[i+k][j+l] and 0x0000FF00) shr 8)*gX[k][l])
                            blueX += ((matrix[i+k][j+l] and 0x000000FF)*gX[k][l])

                            redY += (((matrix[i+k][j+l] and 0x00FF0000) shr 16)*gY[k][l])
                            greenY += (((matrix[i+k][j+l] and 0x0000FF00) shr 8)*gY[k][l])
                            blueY += ((matrix[i+k][j+l] and 0x000000FF)*gY[k][l])
                        }
                    }

                    var red = Math.sqrt((redX * redX + redY * redY).toDouble()).toLong()
                    var green = Math.sqrt((greenX * greenX + greenY * greenY).toDouble()).toLong()
                    var blue = Math.sqrt((blueX * blueX + blueY * blueY).toDouble()).toLong()

                    /*if(i in 1 until height && j in 1 until width) {
                        if ((matrix[i - 1][j - 1] and 0x00FF0000) shr 16 > red && (matrix[i + 1][j + 1] and 0x00FF0000) shr 16 > red)
                            red = 0
                        if ((matrix[i - 1][j - 1] and 0x0000FF00) shr 8 > green && (matrix[i + 1][j + 1] and 0x0000FF00) shr 8 > green)
                            green = 0
                        if ((matrix[i - 1][j - 1] and 0x000000FF) > blue && (matrix[i + 1][j + 1] and 0x000000FF) > blue)
                            blue = 0
                    }*/

                    red = if (red > 200) 255 else red
                    red = if (red < 60) 0 else red

                    green = if (green > 200) 255 else green
                    green = if (green < 60) 0 else green

                    blue = if (blue > 200) 255 else blue
                    blue = if (blue < 60) 0 else blue

                    newPixels[n]= ((red shl 16) or (green shl 8) or blue or 0xFF000000).toInt()
                    redX = 0
                    greenX = 0
                    blueX = 0
                    redY = 0
                    greenY = 0
                    blueY = 0
                    n++
                }
            }

            newBitmap.setPixels(newPixels, 0, width,0, 0, width, height)
        }

        return newBitmap
    }

    fun embossFilter(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            val newWidth = width + 2
            val newHeight = height + 2
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            val kernel = Array(3) { Array(3) { 0L } }
            kernel[0] = arrayOf(-2, -1, 0)
            kernel[1] = arrayOf(-1, 1, 1)
            kernel[2] = arrayOf(0, 1, 2)

            for (i in 0 until width * height) {
                srcPixels[i] = srcPixels[i]
            }

            val matrix = Array(newHeight) { Array(newWidth) { 0 } }
            var n = 0
            for (i in 1..height) {
                for (j in 1..width) {
                    matrix[i][j] = srcPixels[n]
                    n++
                }
            }

            matrix[0][0] = srcPixels[0]
            matrix[0][newWidth - 1] = srcPixels[height * width - 1]
            matrix[newHeight - 1][0] = srcPixels[height * width - width]
            matrix[newHeight - 1][newWidth - 1] = srcPixels[height * width - 1]

            for (i in 0 until width) {
                matrix[0][i + 1] = srcPixels[i]
            }
            for (i in 1 until width + 1) {
                matrix[newHeight - 1][newWidth - i - 1] = srcPixels[height * width - i - 1]
            }
            for (i in 0 until height) {
                matrix[i + 1][0] = srcPixels[i * width]
            }
            for (i in 0 until height) {
                matrix[i + 1][newWidth - 1] = srcPixels[i * width + width - 1]
            }

            var red = 0L
            var green = 0L
            var blue = 0L
            n = 0
            val size = kernel.size
            for (i in 1..height) {
                for (j in 1..width) {
                    for (k in 0 until size) {
                        for (l in 0 until size) {
                            red += (((matrix[i + k - 1][j + l - 1] and 0x00FF0000) shr 16) * kernel[k][l])
                            green += (((matrix[i + k - 1][j + l - 1] and 0x0000FF00) shr 8) * kernel[k][l])
                            blue += ((matrix[i + k - 1][j + l - 1] and 0x000000FF) * kernel[k][l])
                        }
                    }

                    red = if (red > 255) 255 else red
                    red = if (red < 0) 0 else red

                    green = if (green > 255) 255 else green
                    green = if (green < 0) 0 else green

                    blue = if (blue > 255) 255 else blue
                    blue = if (blue < 0) 0 else blue

                    newPixels[n] = ((red shl 16) or (green shl 8) or blue or 0xFF000000).toInt()
                    red = 0
                    green = 0
                    blue = 0
                    n++
                }
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }
        return newBitmap
    }

    fun blurFilter(imageBitmap: Bitmap?, sigma: Double): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height

            val size = (2 * Math.ceil(sigma) + 1).toInt()
            val radius = size / 2
            val newWidth = width + size - 1
            val newHeight = height + size - 1
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            val kernel = Array(size) {Array(size) {0.0} }
            var sum = 0.0
            for (i in -radius..radius)
                for (j in -radius..radius) {
                    kernel[i + radius][j + radius] =
                        1.0 / (2.0 * Math.PI * sigma * sigma) * Math.exp(-(i * i + j * j) / (2.0 * sigma * sigma))
                    sum += kernel[i + radius][j + radius]
                }
            for (i in 0 until size)
                for (j in 0 until size) {
                    kernel[i][j] /= sum
                }

            val matrix = Array(newHeight) {Array(newWidth) {0}}
            var n = 0
            for (i in radius until height + radius) {
                for (j in radius until width + radius) {
                    matrix[i][j] = srcPixels[n]
                    n++
                }
            }

            for (k in 0 until radius) {
                for (i in 0 until radius) {
                    matrix[k][i] = srcPixels[0]
                    matrix[k][newWidth + i - radius] = srcPixels[width - 1]
                    matrix[newHeight + k - radius][i] = srcPixels[height*width-width]
                    matrix[newHeight + k - radius][newWidth + i - radius] = srcPixels[height*width-1]
                }

                for (i in radius until width + radius)
                    matrix[k][i] = srcPixels[i - radius]

                for (i in radius until width + radius)
                    matrix[newHeight + k - radius][i] = srcPixels[width * (height - 1) + i - radius]

                for (i in 0 until height)
                    matrix[i + radius][k] = srcPixels[i * width]

                for (i in 1 until height + 1)
                    matrix[i + radius - 1][newWidth + k - radius] = srcPixels[i * width - radius - 1]
            }

            var red = 0.0
            var green = 0.0
            var blue = 0.0
            n = 0
            for (i in 0 until height) {
                for (j in 0 until width) {
                    for (k in 0 until size) {
                        for (l in 0 until size) {
                            red += (((matrix[i+k][j+l] and 0x00FF0000) shr 16)*kernel[k][l])
                            green += (((matrix[i+k][j+l] and 0x0000FF00) shr 8)*kernel[k][l])
                            blue += ((matrix[i+k][j+l] and 0x000000FF)*kernel[k][l])
                        }
                    }

                    red = if (red > 255) 255.0 else red
                    red = if (red < 0) 0.0 else red

                    green = if (green > 255) 255.0 else green
                    green = if (green < 0) 0.0 else green

                    blue = if (blue > 255) 255.0 else blue
                    blue = if (blue < 0) 0.0 else blue

                    newPixels[n] = ((red.toLong() shl 16) or (green.toLong() shl 8) or blue.toLong() or 0xFF000000).toInt()
                    red = 0.0
                    green = 0.0
                    blue = 0.0
                    n++
                }
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }

    fun unsharpMashing(imageBitmap: Bitmap?, radius: Double, amount: Double, threshold: Int): Bitmap? {
        var newBitmap: Bitmap? = null
        val blurBitmap = blurFilter(imageBitmap, radius)
        if (imageBitmap != null && blurBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val blurPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)

            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)
            blurBitmap.getPixels(blurPixels, 0, width, 0, 0, width, height)

            for (i in 0 until width * height) {
                val srcPixel = srcPixels[i].toLong()
                val srcRed = ((srcPixel and 0x00FF0000) shr 16)
                val srcGreen = ((srcPixel and 0x0000FF00) shr 8)
                val srcBlue = (srcPixel and 0x000000FF)

                val blurPixel = blurPixels[i].toLong()
                var redMask = srcRed - ((blurPixel and 0x00FF0000) shr 16)
                var greenMask = srcGreen - ((blurPixel and 0x0000FF00) shr 8)
                var blueMask = srcBlue - (blurPixel and 0x000000FF)

                redMask = if (Math.abs(2 * redMask) < threshold) 0 else redMask
                greenMask = if (Math.abs(2 * greenMask) < threshold) 0 else greenMask
                blueMask = if (Math.abs(2 * blueMask) < threshold) 0 else blueMask

                var red = (srcRed + redMask * amount).toLong()
                var green = (srcGreen + greenMask * amount).toLong()
                var blue = (srcBlue + blueMask * amount).toLong()

                red = if (red > 255) 255 else red
                red = if (red < 0) 0 else red

                green = if (green > 255) 255 else green
                green = if (green < 0) 0 else green

                blue = if (blue > 255) 255 else blue
                blue = if (blue < 0) 0 else blue

                newPixels[i] = ((red shl 16) or (green shl 8) or blue or 0xFF000000).toInt()
            }

            newBitmap!!.setPixels(newPixels, 0, width, 0, 0, width, height)
        }
        return newBitmap
    }

}