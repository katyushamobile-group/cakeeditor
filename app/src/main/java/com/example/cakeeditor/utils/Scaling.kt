package com.example.cakeeditor.utils

object Scaling {
    fun resizeBilinear(pixels: IntArray, w: Int, h: Int, w2: Int, h2: Int): IntArray {
        val temp = IntArray(w2 * h2)
        var a: Int
        var b: Int
        var c: Int
        var d: Int
        var x: Int
        var y: Int
        var index: Int
        val xRatio = (w - 1).toFloat() / w2
        val yRatio = (h - 1).toFloat() / h2
        var xDiff: Float
        var yDiff: Float
        var blue: Float
        var red: Float
        var green: Float
        var offset = 0
        for (i in 0 until h2) {
            for (j in 0 until w2) {
                x = (xRatio * j).toInt()
                y = (yRatio * i).toInt()
                xDiff = xRatio * j - x
                yDiff = yRatio * i - y
                index = y * w + x
                a = pixels[index]
                b = pixels[index + 1]
                c = pixels[index + w]
                d = pixels[index + w + 1]

                // blue element
                // Yb = Ab(1-w)(1-h) + Bb(w)(1-h) + Cb(h)(1-w) + Db(wh)
                blue =
                    (a and 0xff).toFloat() * (1 - xDiff) * (1 - yDiff) + (b and 0xff).toFloat() * xDiff * (1 - yDiff) +
                            (c and 0xff).toFloat() * yDiff * (1 - xDiff) + (d and 0xff) * (xDiff * yDiff)

                // green element
                // Yg = Ag(1-w)(1-h) + Bg(w)(1-h) + Cg(h)(1-w) + Dg(wh)
                green =
                    (a shr 8 and 0xff).toFloat() * (1 - xDiff) * (1 - yDiff) + (b shr 8 and 0xff).toFloat() * xDiff * (1 - yDiff) +
                            (c shr 8 and 0xff).toFloat() * yDiff * (1 - xDiff) + (d shr 8 and 0xff) * (xDiff * yDiff)

                // red element
                // Yr = Ar(1-w)(1-h) + Br(w)(1-h) + Cr(h)(1-w) + Dr(wh)
                red =
                    (a shr 16 and 0xff).toFloat() * (1 - xDiff) * (1 - yDiff) + (b shr 16 and 0xff).toFloat() * xDiff * (1 - yDiff) +
                            (c shr 16 and 0xff).toFloat() * yDiff * (1 - xDiff) + (d shr 16 and 0xff) * (xDiff * yDiff)

                temp[offset++] = -0x1000000 or // hardcode alpha

                        (red.toInt() shl 16 and 0xff0000) or
                        (green.toInt() shl 8 and 0xff00) or
                        blue.toInt()
            }
        }
        return temp
    }
}