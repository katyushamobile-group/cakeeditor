package com.example.cakeeditor.utils

class Vertex() {
    var x: Int = 0
    var y: Int = 0
    var visited: Int = 0
    var to_watch:Int = 0
    var dist_from_start:Int = 0
    var func:Int = 0
    var parent_x:Int = 0
    var parent_y:Int = 0
}