package com.example.cakeeditor.utils

import android.graphics.Bitmap


object ColorFilter {
    fun negativeFilter(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            for (i in 0 until srcPixels.size) {
                newPixels[i] = (0xFF000000 or (0xFFFFFFFF - srcPixels[i].toLong())).toInt()
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }

    fun redFilter(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            for (i in 0 until srcPixels.size) {
                val pixel: Long = srcPixels[i].toLong()
                val red = pixel and 0x00FF0000

                val newPixel = 0xFF000000 or red
                newPixels[i] = newPixel.toInt()
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }

    fun greenFilter(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            for (i in 0 until srcPixels.size) {
                val pixel: Long = srcPixels[i].toLong()
                val green = pixel and 0x0000FF00

                val newPixel = 0xFF000000 or green
                newPixels[i] = newPixel.toInt()
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }

    fun blueFilter(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            for (i in 0 until srcPixels.size) {
                val pixel: Long = srcPixels[i].toLong()
                val blue = pixel and 0x000000FF

                val newPixel = 0xFF000000 or blue
                newPixels[i] = newPixel.toInt()
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }

    fun blackWhiteFilter(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            for (i in 0 until srcPixels.size) {
                val pixel: Long = srcPixels[i].toLong()
                val red: Double = ((pixel and 0x00FF0000) shr 16).toDouble()
                val green: Double = ((pixel and 0x0000FF00) shr 8).toDouble()
                val blue: Double = (pixel and 0x000000FF).toDouble()

                val color = (red + green + blue) / 3
                val newPixel = 0xFF000000 or (color.toLong() shl 16) or (color.toLong() shl 8) or color.toLong()
                newPixels[i] = newPixel.toInt()
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }

    fun coldFilter(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            for (i in 0 until srcPixels.size) {
                val pixel: Long = srcPixels[i].toLong()
                val red = ((pixel and 0x00FF0000) shr 16)
                var green = ((pixel and 0x0000FF00) shr 8)
                var blue = (pixel and 0x000000FF)

                blue += ((255 - blue) * 0.25).toLong()
                green += ((255 - green) * 0.05).toLong()
                blue = if (blue > 255) 255 else blue
                green = if (green > 255) 255 else green

                val newPixel = 0xFF000000 or (red shl 16) or (green shl 8) or blue
                newPixels[i] = newPixel.toInt()
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }

    fun warmFilter(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            for (i in 0 until srcPixels.size) {
                val pixel: Long = srcPixels[i].toLong()
                var red = ((pixel and 0x00FF0000) shr 16)
                var green = ((pixel and 0x0000FF00) shr 8)
                val blue = (pixel and 0x000000FF)
                red += ((255 - red) * 0.25).toLong()
                green += ((255 - green) * 0.05).toLong()
                //blue += ((255 - blue) * 0.5).toLong()
                //red = (red*1.2).toLong()

                red = if (red > 255) 255 else red
                green = if (green > 255) 255 else green
                //blue = if (blue > 255) 255 else blue

                val newPixel = 0xFF000000 or (red shl 16) or (green shl 8) or blue
                newPixels[i] = newPixel.toInt()
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }

    fun xRayFilter(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            val patternBitmap = negativeFilter(blackWhiteFilter(imageBitmap))

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            patternBitmap!!.getPixels(srcPixels, 0, width, 0, 0, width, height)

            for (i in 0 until srcPixels.size) {
                val pixel: Long = srcPixels[i].toLong()
                val red = ((pixel and 0x00FF0000) shr 16)
                var green = ((pixel and 0x0000FF00) shr 8)
                var blue = (pixel and 0x000000FF)

                blue += (blue * 1.08).toLong()
                green += (green * 1.03).toLong()
                blue = if (blue > 255) 255 else blue
                green = if (green > 255) 255 else green

                val newPixel = 0xFF000000 or (red shl 16) or (green shl 8) or blue
                newPixels[i] = newPixel.toInt()
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }

    fun sepiaFilter(imageBitmap: Bitmap?): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            for (j in 0 until srcPixels.size) {
                val pixel: Long = srcPixels[j].toLong()


                var red = ((pixel and 0x00FF0000) shr 16)
                var green = ((pixel and 0x0000FF00) shr 8)
                var blue = (pixel and 0x000000FF)

                val y = 0.299 * red + 0.587 * green + 0.114 * blue
                val i = 51
                red = (y + 0.956 * i).toLong()
                green = (y - 0.272 * i).toLong()
                blue = (y - 1.105 * i).toLong()

                red = if (red > 255) 255 else red
                green = if (green < 0) 0 else green
                blue = if (blue < 0) 0 else blue
                /*val newRed = 0.403 * red + 0.769 * green + 0.189 * blue
                val newGreen = 0.346 * red + 0.686 * green + 0.168 * blue
                val newBlue = 0.272 * red + 0.534 * green + 0.131 * blue

                red = if (newRed > 255) 255 else newRed.toLong()
                green = if (newGreen > 255) 255 else newGreen.toLong()
                blue = if (newBlue > 255) 255 else newBlue.toLong()*/

                val newPixel: Long = 0xFF000000 or (red shl 16) or (green shl 8) or blue
                newPixels[j] = newPixel.toInt()
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }

    fun setBrightness(imageBitmap: Bitmap?, point: Int): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            for (j in 0 until srcPixels.size) {
                val pixel: Long = srcPixels[j].toLong()

                var red = ((pixel and 0x00FF0000) shr 16) + point
                var green = ((pixel and 0x0000FF00) shr 8) + point
                var blue = (pixel and 0x000000FF) + point

                if (red > 255) red = 255
                else if (red < 0) red = 0

                if (green > 255) green = 255
                else if (green < 0) green = 0

                if (blue > 255) blue = 255
                else if (blue < 0) blue = 0

                val newPixel: Long = 0xFF000000 or (red shl 16) or (green shl 8) or blue
                newPixels[j] = newPixel.toInt()
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }

    fun setContrast(imageBitmap: Bitmap?, point: Int): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            val width = imageBitmap.width
            val height = imageBitmap.height
            newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            val newPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            val contrast = Math.pow((100.0 + point) / 100.0, 2.0)

            for (j in 0 until srcPixels.size) {
                val pixel: Long = srcPixels[j].toLong()

                var red = ((pixel and 0x00FF0000) shr 16)
                var green = ((pixel and 0x0000FF00) shr 8)
                var blue = (pixel and 0x000000FF)

                red = (((((red / 255.0) - 0.5)*contrast) + 0.5)*255).toLong()
                green = (((((green / 255.0) - 0.5)*contrast) + 0.5)*255).toLong()
                blue = (((((blue / 255.0) - 0.5)*contrast) + 0.5)*255).toLong()

                if (red > 255) red = 255
                else if (red < 0) red = 0

                if (green > 255) green = 255
                else if (green < 0) green = 0

                if (blue > 255) blue = 255
                else if (blue < 0) blue = 0

                val newPixel: Long = 0xFF000000 or (red shl 16) or (green shl 8) or blue
                newPixels[j] = newPixel.toInt()
            }

            newBitmap.setPixels(newPixels, 0, width, 0, 0, width, height)
        }

        return newBitmap
    }
}