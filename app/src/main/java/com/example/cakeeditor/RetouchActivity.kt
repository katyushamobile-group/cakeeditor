package com.example.cakeeditor

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.widget.ImageView
import android.widget.SeekBar
import com.example.cakeeditor.utils.MatrixFilter
import com.example.cakeeditor.utils.Saving
import kotlinx.android.synthetic.main.activity_retouch.*

class RetouchActivity : AppCompatActivity() {

    private var imageBitmap: Bitmap? = null
    private var resultBitmap: Bitmap? = null
    private var level = 1.0
    private var size = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_retouch)

        val imageUri = Uri.parse(intent.getStringExtra("imageRetouch"))
        imageBitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
        imageRetouchView.setImageBitmap(imageBitmap)
        resultBitmap = imageBitmap

        size = Math.min(imageBitmap!!.width, imageBitmap!!.height) / 50
        size = if (size > 0) size else 1
        val startSize = size
        size = startSize * 2

        imageRetouchView.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            startActivityForResult(intent, 9)
        }

        imageRetouchView.setOnTouchListener { v, event ->
            val action = event.action
            val x = event.x.toInt()
            val y = event.y.toInt()

            if (action == MotionEvent.ACTION_DOWN
                || action == MotionEvent.ACTION_UP
                || action ==  MotionEvent.ACTION_MOVE) {
                resultBitmap = applyEffect(v as ImageView, resultBitmap, x, y)
                imageRetouchView.setImageBitmap(resultBitmap)
            }

            true
        }

        resetButton.setOnClickListener {
            levelSeekBar.progress = 0
            sizeSeekBar.progress = 0
            level = 1.0
            size = startSize
            resultBitmap = imageBitmap
            imageRetouchView.setImageBitmap(resultBitmap)
        }

        levelSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                level = (i * 3).toDouble()
                level = if (level > 0.0) level else 1.0
                level = if (2 * level + 1 > size) size.toDouble() / 2.0 else level
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })

        sizeSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                size = ((i + 2) * startSize)
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_tool, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_ok -> {
            val returnIntent = Intent()
            val newUri = Uri.fromFile(Saving.saveTempFile(resultBitmap, getExternalFilesDir(Environment.DIRECTORY_PICTURES)))
            returnIntent.putExtra("result", newUri.toString())
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun applyEffect(iv: ImageView, imageBitmap: Bitmap?, x: Int, y: Int): Bitmap? {
        if (imageBitmap != null && x >= 0 && y >= 0 && x < iv.width && y < iv.height) {

            val width = imageBitmap.width
            val height = imageBitmap.height

            val projectedX =
                Math.round(x.toDouble() * (width.toDouble() / iv.width.toDouble())).toInt()
            val projectedY =
                Math.round(y.toDouble() * (height.toDouble() / iv.height.toDouble())).toInt()

            val r = size / 2
            if (width > r && height > r && projectedX < width - r && projectedY < height - r) {
                val srcPixels = IntArray(width * height)
                imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

                val newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

                if (projectedX in r until width - r && projectedY in r until height - r) {
                    val maskBitmap =
                        MatrixFilter.blurFilter(Bitmap.createBitmap(
                            imageBitmap,
                            projectedX - r,
                            projectedY - r,
                            size, size), level)
                    val temp = IntArray(size * size)
                    maskBitmap!!.getPixels(temp, 0, size, 0, 0, size, size)

                    newBitmap.setPixels(srcPixels, 0, width, 0, 0, width, height)
                    for(i in 360 downTo 1){
                        val dx = (r * Math.cos(i.toDouble())).toInt()
                        val dy = (r * Math.sin(i.toDouble())).toInt()

                        val x1 = projectedX - dx
                        val y1 = projectedY + dy
                        val y2 = projectedY - dy

                        for(j in x1..(x1+2*dx)) {
                            newBitmap.setPixel(j,y1,maskBitmap.getPixel(j - (projectedX - r),y1 - (projectedY - r)))
                            newBitmap.setPixel(j,y2,maskBitmap.getPixel(j - (projectedX - r),y2 - (projectedY - r)))
                        }
                    }
                    return newBitmap
                }
            }
        }
        return imageBitmap
    }
}
