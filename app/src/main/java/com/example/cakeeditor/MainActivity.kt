package com.example.cakeeditor

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.cakeeditor.utils.Saving
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.IOException

class MainActivity : AppCompatActivity() {

    private val REQUEST_PICTURE_CAPTURE = 1
    private val REQUEST_PHOTO_CAPTURE = 2
    private var currentPath: String? = null
    private var imageUri: Uri? = null
    private var imageBitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        galleryButton.setOnClickListener {
            Dexter.withActivity(this)
                .withPermissions(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            selectPictureInGallery()
                        } else
                            Toast.makeText(applicationContext, "Permission denied", Toast.LENGTH_SHORT).show()
                    }
                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>,
                        token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
        }
        cameraButton.setOnClickListener {
            Dexter.withActivity(this)
                .withPermissions(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            takePhoto()
                        } else
                            Toast.makeText(applicationContext, "Permission denied", Toast.LENGTH_SHORT).show()
                    }
                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>,
                        token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
        }

        startButton.setOnClickListener {
            startProcessing()
        }

    }

    private fun startProcessing() {
        if (imageBitmap == null && imageUri == null) {
            imageBitmap = BitmapFactory.decodeResource(applicationContext.resources, R.drawable.main)
            imageUri = Uri.fromFile(Saving.saveTempFile(imageBitmap, getExternalFilesDir(Environment.DIRECTORY_PICTURES)))
        }
        val sendPictureIntent = Intent(this, ToolActivity::class.java)
        imageUri?.also {
            sendPictureIntent.putExtra("imageUri", imageUri.toString())
            startActivity(sendPictureIntent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_PICTURE_CAPTURE && resultCode == Activity.RESULT_OK) {
            try {
                val pictureUri = data?.data
                imageBitmap = compressBitmap(pictureUri)
                //imageBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, pictureUri)
                imageUri = Uri.fromFile(Saving.saveTempFile(imageBitmap, getExternalFilesDir(Environment.DIRECTORY_PICTURES)))
                imagePreview.setImageBitmap(imageBitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else if (requestCode == REQUEST_PHOTO_CAPTURE && resultCode == Activity.RESULT_OK) {
            try {
                val file = File(currentPath)
                val photoUri = Uri.fromFile(file)
                imageBitmap = compressBitmap(photoUri)
                imagePreview.setImageURI(imageUri)
                //imageBitmap = data?.extras?.get("data") as Bitmap
                imageUri = Uri.fromFile(Saving.saveTempFile(imageBitmap, getExternalFilesDir(Environment.DIRECTORY_PICTURES)))
                imagePreview.setImageBitmap(imageBitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun selectPictureInGallery() {
        val galleryIntent = Intent().apply {
            type = "image/*"
            action = Intent.ACTION_GET_CONTENT
        }
        //Intent.ACTION_PICK
        galleryIntent.resolveActivity(packageManager)?.also {
            startActivityForResult(galleryIntent, REQUEST_PICTURE_CAPTURE)
        }
    }

    private fun takePhoto() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (cameraIntent.resolveActivity(packageManager) != null) {
            val filePath = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            if (filePath != null) {
                val photoFile = Saving.createTempFile(filePath)
                currentPath = photoFile.absolutePath
                val photoUri = FileProvider.getUriForFile(
                    this,
                    "com.example.cakeeditor.fileprovider", photoFile
                )
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(cameraIntent, REQUEST_PHOTO_CAPTURE)
            }
        }
    }

    private fun compressBitmap(uri: Uri?) : Bitmap? {
        if (uri != null) {
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            //val filePath= imageUri.path
            BitmapFactory.decodeStream(contentResolver.openInputStream(uri), null, o)
            val REQUIRED_SIZE = 1000
            var width = o.outWidth
            var height = o.outHeight

            var scale = 1
            while (true) {
                if (width <= REQUIRED_SIZE && height <= REQUIRED_SIZE)
                    break
                width /= 2
                height /= 2
                scale *= 2
            }
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale

            return BitmapFactory.decodeStream(contentResolver.openInputStream(uri), null, o2)
        } else return null
    }

}
