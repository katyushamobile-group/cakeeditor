package com.example.cakeeditor

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.cakeeditor.utils.Saving
import com.example.cakeeditor.utils.Scaling
import kotlinx.android.synthetic.main.activity_scaling.*

class ScalingActivity : AppCompatActivity() {

    private var imageBitmap: Bitmap? = null
    private var resultBitmap: Bitmap? = null
    private var imageUri: Uri? = null

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scaling)
        var unit = 1
        pixelButton.setTextColor(resources.getColor(R.color.colorBackground))
        imageUri = Uri.parse(intent.getStringExtra("imageScaling"))
        imageBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
        resultBitmap = imageBitmap
        sizeView.text = "${resultBitmap!!.width}x${resultBitmap!!.height}"
        imageScalingView.setImageBitmap(imageBitmap)

        resetScalingButton.setOnClickListener {
            resultBitmap = imageBitmap
            sizeView.text = "${resultBitmap!!.width}x${resultBitmap!!.height}"
            imageScalingView.setImageBitmap(resultBitmap)
        }

        pixelButton.setOnClickListener {
            unit = 1
            pixelButton.isEnabled = false
            pixelButton.setTextColor(resources.getColor(R.color.colorBackground))
            percentButton.isEnabled = true
            percentButton.setTextColor(resources.getColor(R.color.colorText))
        }
        percentButton.setOnClickListener {
            unit = 2
            percentButton.isEnabled = false
            percentButton.setTextColor(resources.getColor(R.color.colorBackground))
            pixelButton.isEnabled = true
            pixelButton.setTextColor(resources.getColor(R.color.colorText))
        }

        scaleButton.setOnClickListener {
            if (widthText.text.toString() == "" && heightText.text.toString() == "") {
                Toast.makeText(applicationContext, "Enter numbers", Toast.LENGTH_SHORT).show()
            } else {
                val width: Int
                val height: Int
                if (unit == 1) {
                    width = widthText.text.toString().toInt()
                    height = heightText.text.toString().toInt()
                } else {
                    width = (widthText.text.toString().toDouble() / 100.0 * resultBitmap!!.width ).toInt ()
                    height = (heightText.text.toString().toDouble() / 100.0 * resultBitmap!!.height ).toInt ()

                }
                if (width > 2000 && height > 2000) {
                    Toast.makeText(applicationContext, "The numbers are very large. Try again", Toast.LENGTH_SHORT).show()
                } else if (width < 10 && height < 10){
                    Toast.makeText(applicationContext, "The numbers are very small. Try again", Toast.LENGTH_SHORT).show()
                } else {
                    Thread(Runnable {
                        resultBitmap = scaleImage(resultBitmap, width, height)
                        imageScalingView.post {
                            imageScalingView.setImageBitmap(resultBitmap)
                            sizeView.text = "${resultBitmap!!.width}x${resultBitmap!!.height}"
                        }
                    }).start()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_tool, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_ok -> {
            val returnIntent = Intent()
            val newUri = Uri.fromFile(Saving.saveTempFile(resultBitmap, getExternalFilesDir(Environment.DIRECTORY_PICTURES)))
            returnIntent.putExtra("result", newUri.toString())
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun scaleImage(imageBitmap: Bitmap?, newWidth: Int, newHeight: Int): Bitmap? {
        var newBitmap: Bitmap? = null
        if (imageBitmap != null) {
            // Вычисляем ширину и высоту изображения
            val width = imageBitmap.width
            val height = imageBitmap.height

            // высота и ширина для нового битмапа
            //val w = width / 2
            //val h = height / 2

            // создаем новый битмап
            newBitmap = Bitmap.createBitmap( newWidth, newHeight, Bitmap.Config.ARGB_8888)

            val srcPixels = IntArray(width * height)
            imageBitmap.getPixels(srcPixels, 0, width, 0, 0, width, height)

            // Выбор алгоритма (выбрать 1 из двух)
            // Ближайший сосед
            //destPixels = resizePixels(srcPixels, width, height,  newWidth, newHeight)
            // Интерполяцией
            val destPixels = Scaling.resizeBilinear(srcPixels, width, height,  newWidth, newHeight)

            newBitmap.setPixels(destPixels, 0,  newWidth, 0, 0,  newWidth, newHeight)
        }

        return newBitmap
    }

    /*private fun resizePixels(pixels: IntArray, w1: Int, h1: Int, w2: Int, h2: Int): IntArray {
        val temp = IntArray(w2 * h2)
        // EDIT: added +1 to account for an early rounding problem
        val x_ratio = (w1 shl 16) / w2 + 1
        val y_ratio = (h1 shl 16) / h2 + 1
        //int x_ratio = (int)((w1<<16)/w2) ;
        //int y_ratio = (int)((h1<<16)/h2) ;
        var x2: Int
        var y2: Int
        for (i in 0 until h2) {
            for (j in 0 until w2) {
                x2 = j * x_ratio shr 16
                y2 = i * y_ratio shr 16
                temp[i * w2 + j] = pixels[y2 * w1 + x2]
            }
        }
        return temp
    }*/
}
